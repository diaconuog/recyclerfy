package com.inzein.greenow.data.localdb.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.inzein.greenow.data.models.RecEvent
import java.time.OffsetDateTime

@Dao
interface EventDao {

    @Insert
    fun insert(recEvent: RecEvent)

    @Update
    fun update(recEvent: RecEvent)

    @Delete
    fun delete(recEvent: RecEvent)

    @Query("SELECT * FROM events")
    fun getAllEvents() : LiveData<List<RecEvent>>

    @Query("SELECT * FROM events WHERE recyclingEntityId = :recyclingEntityId AND expiringDate >= :currentDate")
    fun getAllActiveEventsByRecyclingCompanyId(recyclingEntityId : String, currentDate : OffsetDateTime) : LiveData<List<RecEvent>>

    @Query("SELECT * FROM events WHERE expiringDate >= :currentDate")
    fun getAllActiveEvents(currentDate: OffsetDateTime) : LiveData<List<RecEvent>>
}