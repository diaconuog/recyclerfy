package com.inzein.greenow.data.localdb.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.inzein.greenow.data.localdb.dao.EventDao
import com.inzein.greenow.data.localdb.dao.RecyclingEntityDao
import com.inzein.greenow.data.localdb.dao.UserDao
import com.inzein.greenow.data.models.RecEvent
import com.inzein.greenow.data.models.RecyclingEntity
import com.inzein.greenow.data.models.User

@Database(entities = arrayOf(User::class, RecyclingEntity::class, RecEvent::class), version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class GreenowRoomDatabase : RoomDatabase() {

    abstract fun eventDao() : EventDao
    abstract fun recyclingEntityDao() : RecyclingEntityDao
    abstract fun userDao() : UserDao

    companion object {

        @Volatile
        private var INSTANCE: GreenowRoomDatabase? = null

        fun getInstance(context: Context): GreenowRoomDatabase {
            val tempInstance =
                INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    GreenowRoomDatabase::class.java,
                    "greenow_database"
                ).addCallback(roomCallback).build()
                INSTANCE = instance
                return instance
            }
        }

        fun destroyInstance() {
            INSTANCE = null
        }


        private val roomCallback:RoomDatabase.Callback = object : RoomDatabase.Callback(){
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)

                //todo this is done when openining database for the first time !!
            }
        }
    }

}