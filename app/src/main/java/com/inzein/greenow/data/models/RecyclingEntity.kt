package com.inzein.greenow.data.models

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.firestore.IgnoreExtraProperties
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Entity(tableName = "recycling_entities")
@Parcelize
@IgnoreExtraProperties
@Keep
data class RecyclingEntity(@PrimaryKey var id:String = "", var name:String = "", var fiscalCode:String = "",
                           var locationAddress:String = "", var locationCityName:String = "",
                           var locationCountyName: String = "", var locationCountryName: String = "",
                           var latitude : Double = 0.0, var longitude: Double = 0.0,
                           var phoneNumber:String = "", var companyLevel:Int = 1, var description: String = "",
                           var email: String = "", var image1 : String = "", var image2 : String = "", var image3: String = "",
                           var entityLink: String = "", var facebookLink: String = "",
                           var recyclingItemsList: String = "", var entityOtherDetails : String = "",
                           var isPickUpEntity : Boolean = false
): Parcelable, Serializable {

    fun toMap() : Map<String, Any> {
        val result = HashMap<String, Any>()
        result["id"] = id
        result["name"] = name
        result["fiscalCode"] = fiscalCode
        result["locationAddress"] = locationAddress
        result["locationCityName"] = locationCityName
        result["phoneNumber"] = phoneNumber
        result["companyLevel"] = companyLevel
        result["description"] = description
        result["email"] = email
        result["image1"] = image1
        result["image2"] = image2
        result["image3"] = image3
        result["entityLink"] = entityLink
        result["facebookLink"] = facebookLink
        result["locationCountyName"] = locationCountyName
        result["locationCountryName"] = locationCountryName
        result["recyclingItemsList"] = recyclingItemsList
        result["entityOtherDetails"] = entityOtherDetails

        return result
    }

}