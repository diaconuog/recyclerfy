package com.inzein.greenow.data.models

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.firestore.IgnoreExtraProperties
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import java.time.OffsetDateTime

// we need ? = null in order to be possible to store null values in db
@Entity(tableName = "events")
@Parcelize
@IgnoreExtraProperties
@Keep
data class RecEvent(@PrimaryKey val id:String= "", val name:String= "", val shortDescription:String= "",
                    val longDescription: String = "",
                    val recyclingEntityId:String= "", val expiringDate: OffsetDateTime? = null,
                    var locationCityName:String = "", var latitude : Double = 0.0, var longitude: Double = 0.0,
                    var image1 : String = "", var image2 : String = "", var image3: String = "",
                    var recEventLink: String = "", var facebookLink: String = "",
                    var email : String = ""
): Parcelable, Serializable {

    fun toMap() : Map<String, Any> {
        val result = HashMap<String, Any>()
        result.put("id", id)
        result.put("name", name)
        result.put("description", shortDescription)
        result.put("recyclingEntityId", recyclingEntityId)
        result.put("expiringDate", expiringDate!!)
        result.put("locationCityName", locationCityName)
        result.put("latitude", latitude)
        result.put("longitude", longitude)
        result.put("image1", image1)
        result.put("image2", image2)
        result.put("image3", image3)
        result.put("recEventLink", recEventLink)
        result.put("facebookLink", facebookLink)
        result.put("email", email)


        return result;
    }
}