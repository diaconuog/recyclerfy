package com.inzein.greenow.data.firebase

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.inzein.greenow.data.models.RecEvent

class FirebaseEventsSource: FirebaseSourceBase() {

    private var eventsInMyCity: MutableLiveData<List<RecEvent>> = MutableLiveData()
    private var eventsInMyCounty: MutableLiveData<List<RecEvent>> = MutableLiveData()
    private var eventsInMyCountry: MutableLiveData<List<RecEvent>> = MutableLiveData()
    private var recEvent: MutableLiveData<RecEvent> = MutableLiveData()
    private var allRecEvents : MutableLiveData<List<RecEvent>> = MutableLiveData()


    /*
    *   This will hear any updates from Firestore Cloud
    */
    fun getAllEventsInMyCity(locationCityName: String): LiveData<List<RecEvent>>{
        firebaseFirestoreDatabase.collection("rec_events")
            .whereEqualTo("locationCityName", locationCityName)
            .addSnapshotListener { snapshot, firebaseFirestoreException ->
                if(firebaseFirestoreException != null){
                    Log.e("FirebaseFirestoreRE", "Listen faieled" )
                    return@addSnapshotListener
                }

                //if no exeption encounter
                if (snapshot != null){
                    //populate snapshot
                    val allRecEvents : MutableList<RecEvent> = mutableListOf()
                    val documents = snapshot.documents
                    documents.forEach{
                        val event = it.toObject(RecEvent::class.java)
                        if (event != null){
                            allRecEvents.add(event)
                        }
                    }
                    eventsInMyCity.value = allRecEvents
                }
            }

        return eventsInMyCity
    }

    fun getAllEventsInMyCounty(locationCountyName: String): LiveData<List<RecEvent>>{
        firebaseFirestoreDatabase.collection("rec_events")
            .whereEqualTo("locationCountyName", locationCountyName)
            .addSnapshotListener { snapshot, firebaseFirestoreException ->
                if(firebaseFirestoreException != null){
                    Log.e("FirebaseFirestoreRE", "Listen faieled" )
                    return@addSnapshotListener
                }

                //if no exeption encounter
                if (snapshot != null){
                    //populate snapshot
                    val allRecEvents : MutableList<RecEvent> = mutableListOf()
                    val documents = snapshot.documents
                    documents.forEach{
                        val event = it.toObject(RecEvent::class.java)
                        if (event != null){
                            allRecEvents.add(event)
                        }
                    }
                    eventsInMyCounty.value = allRecEvents
                }
            }

        return eventsInMyCounty
    }

    fun getAllEventsInMyContry(locationCountryName: String): LiveData<List<RecEvent>>{
        firebaseFirestoreDatabase.collection("rec_events")
            .whereEqualTo("locationCountryName", locationCountryName)
            .addSnapshotListener { snapshot, firebaseFirestoreException ->
                if(firebaseFirestoreException != null){
                    Log.e("FirebaseFirestoreRE", "Listen faieled" )
                    return@addSnapshotListener
                }

                //if no exeption encounter
                if (snapshot != null){
                    //populate snapshot
                    val allRecEvents : MutableList<RecEvent> = mutableListOf()
                    val documents = snapshot.documents
                    documents.forEach{
                        val event = it.toObject(RecEvent::class.java)
                        if (event != null){
                            allRecEvents.add(event)
                        }
                    }
                    eventsInMyCountry.value = allRecEvents
                }
            }

        return eventsInMyCountry
    }

    fun getAllRecyclingEvents() : LiveData<List<RecEvent>>{
        firebaseFirestoreDatabase.collection("rec_events")
            .addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                if (firebaseFirestoreException != null){
                    Log.e("FirebaseFirestoreRE", "Listen faieled" )
                    return@addSnapshotListener
                }

                if (querySnapshot != null){
                    val recEvents: MutableList<RecEvent> = mutableListOf()
                    val documents = querySnapshot.documents
                    documents.forEach {
                        val recEvent = it.toObject(RecEvent::class.java)
                        if (recEvent != null){
                            recEvents.add(recEvent)
                        }
                    }
                    allRecEvents.value = recEvents
                }
            }
        return allRecEvents
    }

    fun getRecEventById(id: String): LiveData<RecEvent>{
        firebaseFirestoreDatabase.collection("rec_events")
            .document(id)
            .addSnapshotListener { snapshot, firebaseFirestoreException ->
                recEvent.value = snapshot?.toObject(RecEvent::class.java)!!
            }

        return recEvent
    }
}