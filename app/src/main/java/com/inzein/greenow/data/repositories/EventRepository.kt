package com.inzein.greenow.data.repositories

import androidx.lifecycle.LiveData
import com.inzein.greenow.data.firebase.FirebaseEventsSource
import com.inzein.greenow.data.models.RecEvent


class EventRepository(
    private val firebaseEventsSource: FirebaseEventsSource
) {

    fun getAllEventsInMyCity(locationCityName : String): LiveData<List<RecEvent>> {
        return firebaseEventsSource.getAllEventsInMyCity(locationCityName)
    }

    fun getAllEventsInMyCounty(locationCountyName : String): LiveData<List<RecEvent>> {
        return firebaseEventsSource.getAllEventsInMyCity(locationCountyName)
    }

    fun getAllEventsInMyCountry(locationCountryName : String): LiveData<List<RecEvent>> {
        return firebaseEventsSource.getAllEventsInMyCity(locationCountryName)
    }

    fun getAllEvents(): LiveData<List<RecEvent>> = firebaseEventsSource.getAllRecyclingEvents()

    fun getEventById(id: String): LiveData<RecEvent> = firebaseEventsSource.getRecEventById(id)
}