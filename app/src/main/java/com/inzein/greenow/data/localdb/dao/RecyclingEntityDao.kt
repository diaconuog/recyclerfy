package com.inzein.greenow.data.localdb.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.inzein.greenow.data.models.RecyclingEntity

@Dao
interface RecyclingEntityDao {

    @Insert
    fun insert(recyclingEntity: RecyclingEntity)

    @Update
    fun update(recyclingEntity: RecyclingEntity)

    @Delete
    fun delete(recyclingEntity: RecyclingEntity)

    @Query("SELECT * FROM recycling_entities")
    fun getAllRecyclingCompanies() : LiveData<List<RecyclingEntity>>

}