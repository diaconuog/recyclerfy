package com.inzein.greenow.data.models

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.firestore.IgnoreExtraProperties
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Entity(tableName = "reviews")
@Parcelize
@IgnoreExtraProperties
@Keep
data class Review (@PrimaryKey var id: String = "", var userid : String = "", var recyclingEntityId: String = "",
                   var recyclingEntityType : Int = 1, var userProfilePictureUrl : String = "",
                   var rating : Int = RATING_FIVE_STARS, var message: String = ""
): Parcelable, Serializable {

    companion object {
        const val RATING_ONE_STAR : Int = 1
        const val RATING_TWO_STARS : Int = 2
        const val RATING_THREE_STARS : Int = 3
        const val RATING_FOUR_STARS : Int = 4
        const val RATING_FIVE_STARS : Int = 5
    }

    fun toMap() : Map<String, Any> {
        val result = HashMap<String, Any>()

        result.put("id", id)
        result.put("userid", userid)
        result.put("recyclingEntityId", recyclingEntityId)
        result.put("recyclingEntityType", recyclingEntityType)
        result.put("userProfilePictureUrl", userProfilePictureUrl)
        result.put("rating", rating)
        result.put("message", message)

        return result
    }
}