package com.inzein.greenow.data.firebase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.inzein.greenow.data.models.Review
import io.reactivex.Completable

class FirebaseReviewsSource : FirebaseSourceBase() {

    private var recycleEntityReviews: MutableLiveData<List<Review>> = MutableLiveData()


    fun getAllReviewsByRecycleEntityId(recycleEntityId : String): LiveData<List<Review>> {
        firebaseFirestoreDatabase.collection("reviews")
            .whereEqualTo("recyclingEntityId", recycleEntityId)
            .addSnapshotListener { snapshot, firebaseFirestoreException ->

                if (firebaseFirestoreException != null) {
                    return@addSnapshotListener
                }

                if (snapshot != null) {

                    val reviewList: MutableList<Review> = mutableListOf()
                    val documents = snapshot.documents
                    documents.forEach {
                        val review = it.toObject(Review::class.java)
                        if (review != null) {
                            reviewList.add(review)
                        }
                    }

                    recycleEntityReviews.value = reviewList
                }
            }

        return  recycleEntityReviews
    }

    fun addReview(userid : String, recyclingEntityId: String,
                  recyclingEntityType : Int, userProfilePictureUrl : String,
                  rating : Int, message: String ) = Completable.create{ emitter ->

        val reviewNewId : String = firebaseFirestoreDatabase.collection("reviews").document().id

        val newReview = Review(reviewNewId, userid, recyclingEntityId, recyclingEntityType, userProfilePictureUrl, rating, message)

        firebaseFirestoreDatabase.collection("reviews")
            .document(reviewNewId)
            .set(newReview.toMap())
            .addOnSuccessListener {
                if (!emitter.isDisposed){
                    emitter.onComplete()
                }
            }
            .addOnFailureListener {e ->
                if (!emitter.isDisposed){
                    emitter.onError(e)
                }
            }

    }
}