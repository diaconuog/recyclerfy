package com.inzein.greenow.data.firebase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.inzein.greenow.data.models.User
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.GoogleAuthProvider
import io.reactivex.Completable

class FirebaseUsersSource: FirebaseSourceBase() {


    private var currentUserDataFromFirestore : MutableLiveData<User> = MutableLiveData()

    fun loginUserByEmailAndPassword(email: String, password : String) = Completable.create { emitter ->
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (!emitter.isDisposed){
                if (it.isSuccessful){
                    emitter.onComplete()
                } else {
                    emitter.onError(it.exception!!)
                }
            }
        }
    }

    fun registerUserByFirebase(email: String, password: String) = Completable.create{ emitter ->
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener {
            if(!emitter.isDisposed){
                if (it.isSuccessful){
                    emitter.onComplete()
                }else {
                    emitter.onError(it.exception!!)
                }
            } else {
                emitter.onError(it.exception!!)
            }
        }
    }

    fun firebaseSignInWithGoogle(acct: GoogleSignInAccount) = Completable.create{ emitter ->
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (it.result?.additionalUserInfo?.isNewUser!!) {
                        emitter.onComplete()
                    } else {
                        if (it.result?.additionalUserInfo != null){
                            emitter.onComplete()
                        } else {
                            emitter.onError(it.exception!!)
                        }
                    }
                } else {
                    emitter.onError(it.exception!!)
                }
        }
    }

    fun handleFacebookAccessToken(token: AccessToken) = Completable.create { emitter ->
        val credential = FacebookAuthProvider.getCredential(token.token)
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener { task ->

                //TODO THAT IS FALS WHY? NO COREECT LIVEDATA
//                if (!emitter.isDisposed) {
                    if (task.isSuccessful) {
                        emitter.onComplete()
                    } else {
                       emitter.onError(task.exception!!)
                    }
//                }
            }
    }

    fun addUserToFirestore(firstName: String?, lastName: String?)= Completable.create { emitter ->

        val user = User(firebaseAuth.uid.toString(), firstName ?: "" , lastName ?: "",
            firebaseAuth.currentUser?.email.toString(), "", "",
            0, false,false )

        firebaseFirestoreDatabase.collection("users")
            .document(user.userId)
            .set(user.toMap())
            .addOnSuccessListener { _ ->
                if (!emitter.isDisposed){
                    emitter.onComplete()
                }
            }
            .addOnFailureListener { e ->
                if (!emitter.isDisposed){
                    emitter.onError(e)
                }
            }
    }

    fun addUserToFirestore() = Completable.create { emitter ->


        val user = User(firebaseAuth.uid.toString(), "" , "",
            firebaseAuth.currentUser?.email.toString(), "", "",
            0, false,false )

        firebaseFirestoreDatabase.collection("users")
            .document(user.userId)
            .set(user.toMap())
            .addOnSuccessListener { _ ->
                if (!emitter.isDisposed){
                    emitter.onComplete()
                }
            }
            .addOnFailureListener { e ->
                if (!emitter.isDisposed){
                    emitter.onError(e)
                }
            }
    }

    fun resetFirebasePassword(email: String)= Completable.create { emitter ->
        firebaseAuth.sendPasswordResetEmail(email)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    emitter.onComplete()
                } else {
                    emitter.onError(task.exception!!)
                }
            }
    }

    fun addNewFacebookUserToFirestore(firstName: String, lastName: String){
        val user = User(firebaseAuth.uid.toString(), firstName , lastName,
            firebaseAuth.currentUser?.email.toString(), "", "",
            0, false,false )

        firebaseFirestoreDatabase.collection("users")
            .document(user.userId)
            .set(user.toMap())
    }

    fun addGoogleUserToFirestore() {
        val user = User(firebaseAuth.currentUser?.uid.toString(), firebaseAuth.currentUser?.displayName.toString() , "",
            firebaseAuth.currentUser?.email.toString(), "", "",
            0, false,false )

        firebaseFirestoreDatabase.collection("users")
            .document(user.userId)
            .set(user.toMap())
            .addOnSuccessListener { _ ->
                //todo
            }
            .addOnFailureListener { e ->
                //todo
            }
    }

    fun getCurrentUserDetailsFromFirestore(userUid: String): LiveData<User>{
        firebaseFirestoreDatabase.collection("users")
            .document(userUid)
            .addSnapshotListener { documentSnapshot, firebaseFirestoreException ->
                currentUserDataFromFirestore.value = documentSnapshot?.toObject(User::class.java)
            }

        return currentUserDataFromFirestore
    }


    fun signOut() = firebaseAuth.signOut()

    fun currentUser() = firebaseAuth.currentUser

}