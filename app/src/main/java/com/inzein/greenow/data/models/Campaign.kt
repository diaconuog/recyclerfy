package com.inzein.greenow.data.models

import androidx.annotation.NonNull

data class Campaign(
    @NonNull val campaignId : String, var campaignTitle : String, var campaignDescription: String,
    var campaignLat: String, var campaignLong: String, var recyclingEntityHostId: String,
    var linkAdress: String, var facebookLink: String
) {

    fun toMap() : Map<String, Any> {

        val result = HashMap<String, Any>()
        result.put("campaignId", campaignId)
        result.put("campaignTitle", campaignTitle)
        result.put("campaignDescription", campaignDescription)
        result.put("campaignLat", campaignLat)
        result.put("campaignLong", campaignLong)
        result.put("recyclingEntityHostId", recyclingEntityHostId)
        result.put("linkAdress", linkAdress)
        result.put("facebookLink", facebookLink)

        return result
    }
}