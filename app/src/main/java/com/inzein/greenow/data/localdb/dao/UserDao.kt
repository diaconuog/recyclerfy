package com.inzein.greenow.data.localdb.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.inzein.greenow.data.models.User

@Dao
interface UserDao {

    @Insert
    fun insert(user:User)

    @Update
    fun update(user:User)

    @Delete
    fun delete(user:User)

    @Query("SELECT * FROM users")
    fun getUsers() : LiveData<List<User>>
}