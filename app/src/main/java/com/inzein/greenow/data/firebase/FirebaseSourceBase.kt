package com.inzein.greenow.data.firebase

import android.util.Log
import com.inzein.greenow.ui.splash.SplashActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging

open class FirebaseSourceBase {

    protected val firebaseFirestoreDatabase : FirebaseFirestore by lazy {
        Firebase.firestore
    }

    protected val firebaseAuth : FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }

    init {
        val settings = FirebaseFirestoreSettings.Builder()
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        firebaseFirestoreDatabase.firestoreSettings = settings
    }

    fun initialzeFirebaseMesseging(){
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(SplashActivity.TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                task.result?.token
            })

        FirebaseMessaging.getInstance().isAutoInitEnabled = true
    }

}