package com.inzein.greenow.data.repositories

import com.inzein.greenow.data.firebase.FirebaseSourceBase
import com.inzein.greenow.data.firebase.FirebaseUsersSource
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignInAccount

class UserRepository (
    private val firebaseUsers: FirebaseUsersSource,
    private val firebaseSource : FirebaseSourceBase
){

    fun login(email: String, password: String) = firebaseUsers.loginUserByEmailAndPassword(email, password)

    fun register(email: String, password: String) = firebaseUsers.registerUserByFirebase(email, password)

    fun signInWithGoogle(acct : GoogleSignInAccount) = firebaseUsers.firebaseSignInWithGoogle(acct)

    fun loginWithFacebook(token: AccessToken) = firebaseUsers.handleFacebookAccessToken(token)

    fun currentUser() = firebaseUsers.currentUser()

    fun addNewUserToFirestore(firstName : String?, lastName: String?) = firebaseUsers.addUserToFirestore(firstName, lastName)

    fun addNewUserToFirestoreStandard() = firebaseUsers.addUserToFirestore()

    fun resetFirebasePassword(email: String) = firebaseUsers.resetFirebasePassword(email)

     fun initialzeFirebaseMesseging() = firebaseSource.initialzeFirebaseMesseging()

    fun addNewFacebookUserToFirestore(firstName: String, lastName: String) = firebaseUsers.addNewFacebookUserToFirestore(firstName, lastName)

    fun addGoogleUserToFirestore() = firebaseUsers.addGoogleUserToFirestore()

    fun getCurrentUserDetailsFromFirestore(userUid : String) = firebaseUsers.getCurrentUserDetailsFromFirestore(userUid)

    //async tasks because ROOM doesn't allow operations on the main thread !!
    //because that will freeze our app
//    fun insertUserInLocalDB(user: User){
//        doAsync {
//            userDao.insert(user)
//        }
//    }


}