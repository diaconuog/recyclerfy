package com.inzein.greenow.data.repositories

import androidx.lifecycle.LiveData
import com.inzein.greenow.data.firebase.FirebaseReviewsSource
import com.inzein.greenow.data.models.Review

class ReviewRepository(
    private val firebaseReviewsSource: FirebaseReviewsSource
) {

    fun getAllReviewOfRecycleEntityByid(recycleEntityId : String): LiveData<List<Review>>{
        return firebaseReviewsSource.getAllReviewsByRecycleEntityId(recycleEntityId)
    }

    fun addNewReview(userid : String, recyclingEntityId: String,
                     recyclingEntityType : Int, userProfilePictureUrl : String,
                     rating : Int, message: String){
        firebaseReviewsSource.addReview(userid, recyclingEntityId, recyclingEntityType, userProfilePictureUrl, rating, message)
    }
}