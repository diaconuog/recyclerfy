package com.inzein.greenow.data.repositories

import androidx.lifecycle.LiveData
import com.inzein.greenow.data.firebase.FirebaseRecyclingEntitiesSource
import com.inzein.greenow.data.models.RecyclingEntity

class RecyclingEntityRepository(
    private val firebaseRecyclingEntities: FirebaseRecyclingEntitiesSource
) {

    fun getAllRecyclingEntitiessFromMyCity(locationCityName : String, filterOnlyEntitiesWithPickUp : Boolean): LiveData<List<RecyclingEntity>> {
        return firebaseRecyclingEntities.getAllRecyclingEntitiessFromMyCity(locationCityName, filterOnlyEntitiesWithPickUp)
    }

    fun getAllRecyclingEntitiesFromMyCounty(locationCountyName: String, filterOnlyEntitiesWithPickUp : Boolean): LiveData<List<RecyclingEntity>> {
        return firebaseRecyclingEntities.getAllRecyclingEntitiesFromMyCounty(locationCountyName, filterOnlyEntitiesWithPickUp)
    }

    fun getAllRecyclingEntitiesFromMyCountry(locationCountryName: String, filterOnlyEntitiesWithPickUp : Boolean): LiveData<List<RecyclingEntity>>{
        return firebaseRecyclingEntities.getAllRecyclingEntitiesFromMyCountry(locationCountryName, filterOnlyEntitiesWithPickUp)
    }

    fun getAllRecyclingEntities(): LiveData<List<RecyclingEntity>> =
        firebaseRecyclingEntities.getAllRecyclingEntities()

    fun getRecycleEntityBasedOnId(id: String): LiveData<RecyclingEntity> =
        firebaseRecyclingEntities.getRecycleEntityById(id)

}