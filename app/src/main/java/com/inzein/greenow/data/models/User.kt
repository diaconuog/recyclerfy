package com.inzein.greenow.data.models

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(@PrimaryKey @NonNull val userId: String, val userFirstName:String, val userLastName : String, val email:String,
                val addressLocation:String, val locationStamp: String,
                val userType:Int, val userEmailVerified:Boolean, val  isUserAdmin : Boolean,
                val profilePictureUrl : String = ""
){

    fun toMap() : Map<String, Any> {

        val result = HashMap<String, Any>()
        result.put("userId", userId)
        result.put("userName", userFirstName)
        result.put("email", email)
        result.put("adressLocation", addressLocation)
        result.put("locationStamp", locationStamp)
        result.put("userType", userType)
        result.put("userEmailVerified", userEmailVerified)
        result.put("isUserAdmin", isUserAdmin)
        result.put("profilePictureUrl", profilePictureUrl)

        return result
    }
}