package com.inzein.greenow.data.firebase

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.inzein.greenow.data.models.RecyclingEntity

class FirebaseRecyclingEntitiesSource: FirebaseSourceBase() {

    private var recyclingEntitiesInMyCity : MutableLiveData<List<RecyclingEntity>> = MutableLiveData()
    private var recyclingEntitiesInMyCounty : MutableLiveData<List<RecyclingEntity>> = MutableLiveData()
    private var recyclingEntitiesInMyCountry : MutableLiveData<List<RecyclingEntity>> = MutableLiveData()
    private var allRecyclingEntities : MutableLiveData<List<RecyclingEntity>> = MutableLiveData()
    private var recyclingEntity: MutableLiveData<RecyclingEntity> = MutableLiveData()

    /*
    *   This will hear any updates from Firestore Cloud
    */
    fun getAllRecyclingEntitiessFromMyCity(locationCityName : String, filterOnlyEntitiesWithPickUp : Boolean)
            : LiveData<List<RecyclingEntity>> {
        firebaseFirestoreDatabase.collection("recycling_entities")
            .whereEqualTo("locationCityName", locationCityName)
            .whereEqualTo("isPickUpEntity", filterOnlyEntitiesWithPickUp)
            .addSnapshotListener { snapshot, firebaseFirestoreException ->
                if(firebaseFirestoreException != null){
                    Log.e("FirebaseFirestoreRE", "Listen faieled" )
                    return@addSnapshotListener
                }

                //if no exeption encounter
                if (snapshot != null){
                    //populate snapshot
                    val allRecyclingEntities : MutableList<RecyclingEntity> = mutableListOf()
                    val documents = snapshot.documents
                    documents.forEach{
                        val recyclingEntity = it.toObject(RecyclingEntity::class.java)
                        if (recyclingEntity != null){
                            allRecyclingEntities.add(recyclingEntity)
                        }
                    }
                    recyclingEntitiesInMyCity.value = allRecyclingEntities
                }
            }

        return recyclingEntitiesInMyCity
    }


    fun getAllRecyclingEntitiesFromMyCounty(locationCountyName: String, filterOnlyEntitiesWithPickUp : Boolean)
            : LiveData<List<RecyclingEntity>>{
        firebaseFirestoreDatabase.collection("recycling_entities")
            .whereEqualTo("locationCountyName", locationCountyName)
            .whereEqualTo("isPickUpEntity", filterOnlyEntitiesWithPickUp)
            .addSnapshotListener { snapshot, firebaseFirestoreException ->
                if(firebaseFirestoreException != null){
                    Log.e("FirebaseFirestoreRE", "Listen faieled" )
                    return@addSnapshotListener
                }

                //if no exeption encounter
                if (snapshot != null){
                    //populate snapshot
                    val allRecyclingEntities : MutableList<RecyclingEntity> = mutableListOf()
                    val documents = snapshot.documents
                    documents.forEach{
                        val recyclingEntity = it.toObject(RecyclingEntity::class.java)
                        if (recyclingEntity != null){
                            allRecyclingEntities.add(recyclingEntity)
                        }
                    }
                    recyclingEntitiesInMyCounty.value = allRecyclingEntities
                }
            }

        return recyclingEntitiesInMyCounty
    }

    fun getAllRecyclingEntitiesFromMyCountry(locationCountryName: String, filterOnlyEntitiesWithPickUp : Boolean)
            : LiveData<List<RecyclingEntity>>{
        firebaseFirestoreDatabase.collection("recycling_entities")
            .whereEqualTo("locationCountryName", locationCountryName)
            .whereEqualTo("isPickUpEntity", filterOnlyEntitiesWithPickUp)
            .addSnapshotListener { snapshot, firebaseFirestoreException ->
                if(firebaseFirestoreException != null){
                    Log.e("FirebaseFirestoreRE", "Listen faieled" )
                    return@addSnapshotListener
                }

                //if no exeption encounter
                if (snapshot != null){
                    //populate snapshot
                    val allRecyclingEntities : MutableList<RecyclingEntity> = mutableListOf()
                    val documents = snapshot.documents
                    documents.forEach{
                        val recyclingEntity = it.toObject(RecyclingEntity::class.java)
                        if (recyclingEntity != null){
                            allRecyclingEntities.add(recyclingEntity)
                        }
                    }
                    recyclingEntitiesInMyCountry.value = allRecyclingEntities
                }
            }

        return recyclingEntitiesInMyCountry
    }

    fun getAllRecyclingEntities(): LiveData<List<RecyclingEntity>>{
        firebaseFirestoreDatabase.collection("recycling_entities")
            .addSnapshotListener { snapshot, firebaseFirestoreException ->
                if(firebaseFirestoreException != null){
                    Log.e("FirebaseFirestoreRE", "Listen faieled" )
                    return@addSnapshotListener
                }

                //if no exeption encounter
                if (snapshot != null){
                    //populate snapshot
                    val neededRecycleEntities : MutableList<RecyclingEntity> = mutableListOf()
                    val documents = snapshot.documents
                    documents.forEach{
                        val recyclingEntity = it.toObject(RecyclingEntity::class.java)
                        if (recyclingEntity != null){
                            neededRecycleEntities.add(recyclingEntity)
                        }
                    }
                    allRecyclingEntities.value = neededRecycleEntities
                }
            }
        return allRecyclingEntities
    }

    fun getRecycleEntityById(id: String): LiveData<RecyclingEntity>{
        firebaseFirestoreDatabase.collection("recycling_entities")
            .document(id)
            .addSnapshotListener { snapshot, firebaseFirestoreException ->
                recyclingEntity.value = snapshot?.toObject(RecyclingEntity::class.java)!!
            }

        return recyclingEntity
    }

}