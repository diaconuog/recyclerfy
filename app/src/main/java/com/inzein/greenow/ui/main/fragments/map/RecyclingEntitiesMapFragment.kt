package com.inzein.greenow.ui.main.fragments.map

import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.inzein.greenow.R
import com.inzein.greenow.data.models.RecyclingEntity
import com.inzein.greenow.databinding.FragmentRecyclingEntitiesMapBinding
import com.inzein.greenow.di.kodeinViewModel
import com.inzein.greenow.ui.main.MainActivity
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.firebase.firestore.GeoPoint
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import java.io.IOException

class RecyclingEntitiesMapFragment : Fragment(), KodeinAware, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    companion object{
        @JvmStatic
        fun newInstance(): RecyclingEntitiesMapFragment {
            return RecyclingEntitiesMapFragment()
        }

        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val REQUEST_CHECK_SETTINGS = 2
    }

    override val kodein by kodein()

    private lateinit var activityContext: Activity
    private lateinit var binding: FragmentRecyclingEntitiesMapBinding
    private val recyclingEntitiesMapViewModel: RecyclingEntitiesMapViewModel by kodeinViewModel()

    private lateinit var mMap: GoogleMap
    private var mMapFragment: SupportMapFragment? = null
    private lateinit var lastLocation: Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false
    private lateinit var autocompleteSupportFragment: AutocompleteSupportFragment

    private lateinit var geocoder: Geocoder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityContext = this.activity as Activity

        geocoder = Geocoder(activityContext)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activityContext)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                lastLocation = p0.lastLocation
            }
        }

        createLocationRequest()

        (activity as MainActivity).showGeneralNavigationDrawerType()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_recycling_entities_map,
            container,
            false)

        mMapFragment = childFragmentManager.findFragmentById(R.id.map_view) as SupportMapFragment?
        mMapFragment?.getMapAsync(this)

        autocompleteSupportFragment = childFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment

        autocompleteSupportFragment.setPlaceFields(listOf(Place.Field.ADDRESS))
        autocompleteSupportFragment.setCountry("RO")

        autocompleteSupportFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener{
            override fun onPlaceSelected(p0: Place) {

                val placeLatLng = LatLng(getLocationFromAddress(p0.address.toString()).latitude,
                    getLocationFromAddress(p0.address.toString()).longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(placeLatLng,12f))
            }

            override fun onError(p0: Status) {

            }
        })

        binding.apply {
            lifecycleOwner = this@RecyclingEntitiesMapFragment
        }

        binding.recyclingEntitiesMapViewModel = recyclingEntitiesMapViewModel

        return binding.root
    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!

        mMap.setOnMarkerClickListener(this)

        recyclingEntitiesMapViewModel.getAllRecyclingEntities().observe(viewLifecycleOwner, Observer {

            it.forEach { recyclingEntity ->
                val recyclingEntityLatLot = LatLng(recyclingEntity.latitude, recyclingEntity.longitude)
                placeMarkerOnMap(recyclingEntityLatLot, recyclingEntity)
            }
        })

        val customMarkerInfoWindowAdapter = CustomMarkerInfoWindowAdapter(activityContext)
        mMap.setInfoWindowAdapter(customMarkerInfoWindowAdapter)

        mMap.uiSettings.isZoomControlsEnabled = true

        setUpMap()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                locationUpdateState = false
                startLocationUpdates()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    override fun onResume() {
        super.onResume()
        if (!locationUpdateState){
            startLocationUpdates()
        }
    }


    private fun getLocationFromAddress(address: String): GeoPoint{

        val addressList: List<Address> = geocoder.getFromLocationName(address, 5)
        val location: Address = addressList.get(0)
        return GeoPoint(location.latitude,location.longitude)
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(activityContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activityContext,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        mMap.isMyLocationEnabled = true

        fusedLocationClient.lastLocation.addOnSuccessListener(activityContext) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15.5f))
            }
        }
    }

    private fun placeMarkerOnMap(location: LatLng, recycleEntity: RecyclingEntity) {

        val markerOptions = MarkerOptions().position(location)

        val titleStr = getAddress(location)
        markerOptions.title(titleStr)

       mMap.addMarker(markerOptions).apply {
            setIcon(BitmapDescriptorFactory.fromResource(R.drawable.greenow_pin))
            setTag(recycleEntity)
        }
    }

    private fun getAddress(latLng: LatLng): String {
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            // 2
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            // 3
            if (null != addresses && addresses.isNotEmpty()) {
                address = addresses[0]
                for (i in 0 until address.maxAddressLineIndex) {
                    addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
                }
            }
        } catch (e: IOException) {
            Log.e("RecyclingEntitiesMapFragment", e.localizedMessage!!)
        }

        return addressText
    }

    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(activityContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activityContext,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private fun createLocationRequest() {

        locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(activityContext)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
        task.addOnFailureListener { e ->
            // 6
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    e.startResolutionForResult(activityContext,
                        REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        p0?.showInfoWindow()
        return true
    }

}
