package com.inzein.greenow.ui.login.fragments


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.inzein.greenow.R
import com.inzein.greenow.common.DataBindingHelper.Companion.hideKeyboard
import com.inzein.greenow.databinding.FragmentRegisterBinding
import com.inzein.greenow.di.kodeinViewModel
import com.inzein.greenow.ui.login.AuthListener
import com.inzein.greenow.ui.login.LoginActivity
import com.inzein.greenow.ui.login.replaceFragment
import com.inzein.greenow.ui.main.MainActivity
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein


class RegisterFragment : Fragment(), AuthListener, KodeinAware {

    companion object {
        fun newInstance() : RegisterFragment {
            return RegisterFragment()
        }
    }

    override val kodein by kodein()

    private val registerViewModel: RegisterViewModel by kodeinViewModel()

    private lateinit var binding : FragmentRegisterBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_register,
            container,
            false)

        binding.authViewModel = registerViewModel

        registerViewModel.authListener = this

        binding.backToLoginTv.setOnClickListener {
            (activity as AppCompatActivity).replaceFragment(LoginFragment.newInstance())
        }

        view?.setOnClickListener {
            it.hideKeyboard()
        }


        return binding.root
    }

    override fun onStarted() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun onSuccess() {
        registerViewModel.addNewUserToFirebaseFirestore()
        binding.progressBar.visibility = View.INVISIBLE
        val intent = Intent(activity, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onFailure(message: String) {
        binding.progressBar.visibility = View.INVISIBLE
        if (LoginActivity.isNetworkConnected(activity as AppCompatActivity)) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(context, getString(R.string.warning_no_internet_connection), Toast.LENGTH_LONG).show()
        }
    }


}
