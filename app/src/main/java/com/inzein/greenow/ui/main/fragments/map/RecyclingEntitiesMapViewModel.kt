package com.inzein.greenow.ui.main.fragments.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.inzein.greenow.data.models.RecyclingEntity
import com.inzein.greenow.data.repositories.RecyclingEntityRepository

class RecyclingEntitiesMapViewModel(
    private val repository: RecyclingEntityRepository
): ViewModel() {
    lateinit var locationSearched: String


    fun getAllRecyclingEntities(): LiveData<List<RecyclingEntity>> = repository.getAllRecyclingEntities()

}