package com.inzein.greenow.ui.login.fragments


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.inzein.greenow.R
import com.inzein.greenow.common.DataBindingHelper.Companion.hideKeyboard
import com.inzein.greenow.databinding.FragmentLoginBinding
import com.inzein.greenow.di.kodeinViewModel
import com.inzein.greenow.ui.login.AuthListener
import com.inzein.greenow.ui.login.LoginActivity
import com.inzein.greenow.ui.login.replaceFragment
import com.inzein.greenow.ui.main.MainActivity
import com.facebook.*
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein


class LoginFragment : Fragment(), AuthListener, KodeinAware {

    override val kodein by kodein()
    private val loginViewModel: LoginViewModel by kodeinViewModel()


    companion object {
        private const val GOOGLE_SIGNIN: Int = 1
        private const val TAG : String  = "LoginFragment"

        @JvmStatic
        fun newInstance() : LoginFragment {
            return LoginFragment()
        }
    }


    // facebook login callback manager
    private val callbackManager = CallbackManager.Factory.create()
    private lateinit var binding: FragmentLoginBinding

    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mGoogleSignInOptions: GoogleSignInOptions

    private var selectedFaceebookProfile : Profile? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configureGoogleSignIn()

        view?.setOnClickListener {
            it.hideKeyboard()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_login,
            container,
            false)

        binding.loginViewModel = loginViewModel

        setupUi()

        loginViewModel.authListener = this

        return binding.root
    }

    private fun setupUi(){

        binding.facebookLoginButton.fragment = this
        binding.facebookLoginButton.setPermissions("email", "public_profile")
        binding.facebookLoginButton.registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d(TAG, "facebook:onSuccess:$loginResult")
                loginViewModel.loginWithFacebookAccesToken(loginResult.accessToken)

                if (Profile.getCurrentProfile() == null){
                    object : ProfileTracker(){
                        override fun onCurrentProfileChanged(
                            oldProfile: Profile?,
                            currentProfile: Profile?
                        ) {
                            selectedFaceebookProfile = currentProfile!!
                        }
                    }
                } else {
                    selectedFaceebookProfile = Profile.getCurrentProfile()
                }
            }

            override fun onCancel() {
                Log.d(TAG, "facebook:onCancel")
            }

            override fun onError(error: FacebookException) {
                Log.d(TAG, "facebook:onError", error)
            }
        })

        binding.customFacebookLoginButoon.setOnClickListener {
            binding.facebookLoginButton.performClick()
        }


        binding.customGoogleLoginButton.setOnClickListener {
            googleSignIn()
        }

        binding.recoverPassTvId.setOnClickListener {
            (activity as AppCompatActivity).replaceFragment(ResetPasswordFragment.newInstance())

        }
        binding.registerButtonId.setOnClickListener {
            (activity as AppCompatActivity).replaceFragment(RegisterFragment.newInstance())
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GOOGLE_SIGNIN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                loginViewModel.signInUserWithGoogleAccount(account!!)
                loginViewModel.addGoogleUserToFirestore()
            } catch (e: ApiException) {
                Toast.makeText((activity as AppCompatActivity), getString(R.string.error_google_sign_in_failed), Toast.LENGTH_LONG).show()
            }
        } else {
            // Pass the activity result back to the Facebook SDK
            //todo chef if it trigger this after login facebook attempt
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }

    }

    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .requestProfile()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient((activity as AppCompatActivity), mGoogleSignInOptions)
    }

    private fun googleSignIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, GOOGLE_SIGNIN)
    }

    override fun onStarted() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun onSuccess() {
        binding.progressBar.visibility = View.INVISIBLE
        Toast.makeText(context, getString(R.string.txt_succesfull_login), Toast.LENGTH_LONG).show()

        if (selectedFaceebookProfile != null &&
            (selectedFaceebookProfile?.firstName?.isNotEmpty()!! ||
            selectedFaceebookProfile?.lastName?.isNotEmpty()!!)){
            loginViewModel.addNewFacebookUserToFirestore(selectedFaceebookProfile?.firstName.toString(), selectedFaceebookProfile?.lastName.toString())
        } else {
            loginViewModel.addNewUserToFirebaseFirestore()
        }
        val intent = Intent(activity, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onFailure(message: String) {
        binding.progressBar.visibility = View.INVISIBLE
        if (LoginActivity.isNetworkConnected(activity as AppCompatActivity)) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(context, getString(R.string.warning_no_internet_connection), Toast.LENGTH_LONG).show()
        }
    }
}
