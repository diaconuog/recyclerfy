package com.inzein.greenow.ui.main.fragments.recevents

import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.inzein.greenow.R
import com.inzein.greenow.data.models.RecEvent
import com.inzein.greenow.databinding.FragmentEventsBinding
import com.inzein.greenow.di.kodeinViewModel
import com.inzein.greenow.ui.login.replaceFragment
import com.inzein.greenow.ui.main.MainActivity
import com.inzein.greenow.ui.main.fragments.eventdetails.EventDetailsFragment
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

class EventsNeerMeFragment : Fragment(), KodeinAware, IRecEventItemClickListener {

    companion object{
        @JvmStatic
        fun newInstance(): EventsNeerMeFragment {
            return EventsNeerMeFragment()
        }
    }

    override val kodein by kodein()
    private val eventsViewModel: EventsNearMeViewModel by kodeinViewModel()

    private lateinit var binding : FragmentEventsBinding

    private lateinit var geocoder: Geocoder
    private val recEventsAdapter =
        RecEventsAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        geocoder = Geocoder(context)

        (activity as MainActivity).showGeneralNavigationDrawerType()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_events,
            container,
            false)

        binding.apply {
            lifecycleOwner = this@EventsNeerMeFragment
        }

        binding.eventsViewModel = eventsViewModel

        binding.recyclerView.layoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        binding.recEventsAdapter = recEventsAdapter


        eventsViewModel.getRecEvents().observe(viewLifecycleOwner, Observer {
            recEventsAdapter.submitList(it.toMutableList())
        })

        return binding.root
    }

    override fun onRecEventItemClicked(recEvent: RecEvent) {
        (activity as AppCompatActivity).replaceFragment(EventDetailsFragment.newInstance(recEvent))

    }
}