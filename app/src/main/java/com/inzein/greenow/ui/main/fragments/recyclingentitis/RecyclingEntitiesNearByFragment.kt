package com.inzein.greenow.ui.main.fragments.recyclingentitis

import android.Manifest
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.transition.Fade
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.inzein.greenow.R
import com.inzein.greenow.data.models.RecyclingEntity
import com.inzein.greenow.databinding.FragmentRecyclingCompaniesNearByBinding
import com.inzein.greenow.di.kodeinViewModel
import com.inzein.greenow.ui.login.replaceFragment
import com.inzein.greenow.ui.main.MainActivity
import com.inzein.greenow.ui.main.fragments.recyentitydetails.RecycleEntityDetailsFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein


class RecyclingEntitiesNearByFragment : Fragment(),  KodeinAware, IRecyclerViewItemClickListener{

    companion object{
        @JvmStatic
        fun newInstance() = RecyclingEntitiesNearByFragment()
    }

    override val kodein by kodein()
    private val recyclingEntitiesViewModel: RecyclingEntitiesNearMeViewModel by kodeinViewModel()
    private lateinit var geocoder: Geocoder

    private lateinit var binding: FragmentRecyclingCompaniesNearByBinding
    private val recyclingEntitiesAdapter = RecyclingEntitiesAdapter(this)

    private lateinit var fusedLocationClient: FusedLocationProviderClient


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        geocoder = Geocoder(context)

        (activity as MainActivity).showGeneralNavigationDrawerType()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        super.onCreate(savedInstanceState)

        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_recycling_companies_near_by,
            container,
            false)


        binding.apply {
            lifecycleOwner = this@RecyclingEntitiesNearByFragment
        }

        binding.recyclerCompaniesNearViewModel = recyclingEntitiesViewModel


        //set animation
        val layoutAnimationController = LayoutAnimationController(AnimationUtils.loadAnimation(context,R.anim.recyclerview_item_anim))
        layoutAnimationController.delay = 0.20f
        layoutAnimationController.order = LayoutAnimationController.ORDER_NORMAL

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            layoutAnimation = layoutAnimationController
        }

        binding.recyclingEntitiesAdapter = recyclingEntitiesAdapter

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity as MainActivity)

        if (ActivityCompat.checkSelfPermission(
                activity as MainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                activity as MainActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    if (location != null) {
                        recyclingEntitiesViewModel.location = location
                        recyclingEntitiesViewModel.getRecyclingCompaniesFiltered(geocoder).observe(viewLifecycleOwner, Observer { it ->
                            recyclingEntitiesAdapter.submitList(it.toMutableList())
                        })
                    } else {
                        //Todo ask for current location
                    }
                }
        } else {
            //todo create dialog
        }

        val recycleMaterials = resources.getStringArray(R.array.recycling_materials_array)
        val autoCompleteTextViewAdapter = ArrayAdapter(context!!, android.R.layout.simple_list_item_1, recycleMaterials)
        binding.filterLayout.autocompleteTextview.setAdapter(autoCompleteTextViewAdapter)

        binding.filterLayout.autocompleteTextview.setOnItemClickListener { parent, view, position, id ->
            recyclingEntitiesViewModel.recycleMaterialToFilerFor = recycleMaterials[position]
        }

        binding.filterLayout.autocompleteTextview.setOnClickListener {
            binding.filterLayout.autocompleteTextview.text.clear()
            recyclingEntitiesViewModel.recycleMaterialToFilerFor = ""
            recyclingEntitiesViewModel.applyFilters(geocoder)
        }

        binding.filterLayout.closeFilterButton.setOnClickListener {
            binding.filterLayout.filterParentLayout.visibility = View.GONE
        }

        binding.filterLayout.cityFilterButton.setOnClickListener {
            binding.filterLayout.cityFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_selected, null)
            binding.filterLayout.countyFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_unselected, null)
            binding.filterLayout.countryFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_unselected, null)

            recyclingEntitiesViewModel.filterType = 0
            recyclingEntitiesViewModel.applyFilters(geocoder)

        }

        binding.filterLayout.countyFilterButton.setOnClickListener {
            binding.filterLayout.cityFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_unselected, null)
            binding.filterLayout.countyFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_selected, null)
            binding.filterLayout.countryFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_unselected, null)

            recyclingEntitiesViewModel.filterType = 1
            recyclingEntitiesViewModel.applyFilters(geocoder)
        }

        binding.filterLayout.countryFilterButton.setOnClickListener {
            binding.filterLayout.cityFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_unselected, null)
            binding.filterLayout.countyFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_unselected, null)
            binding.filterLayout.countryFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_selected, null)

            recyclingEntitiesViewModel.filterType = 2
            recyclingEntitiesViewModel.applyFilters(geocoder)

        }

        binding.filterLayout.resetFiltersButton.setOnClickListener {
            binding.filterLayout.cityFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_unselected, null)
            binding.filterLayout.countyFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_unselected, null)
            binding.filterLayout.countryFilterButton.background =
                ResourcesCompat.getDrawable(resources, R.drawable.filter_button_selected, null)

            recyclingEntitiesViewModel.filterType = 2
            binding.filterLayout.onlyPickUpCheckbox.isChecked = false
        }

        binding.filterLayout.onlyPickUpCheckbox.setOnCheckedChangeListener { buttonView, isChecked ->
            recyclingEntitiesViewModel.filterOnlyEntitiesWithPickUp = isChecked
            recyclingEntitiesViewModel.applyFilters(geocoder)
        }

        return binding.root
    }

    override fun onRecyclerviewItemClicked(recyclingEntity: RecyclingEntity) {
        (activity as AppCompatActivity).replaceFragment(RecycleEntityDetailsFragment.newInstance(recyclingEntity))
    }

    fun fadeInAnimate(){
        val fadeOut = Fade(Fade.OUT)
        TransitionManager.beginDelayedTransition(binding.filterLayout.root as ViewGroup, fadeOut)
    }

    fun makeFilterMenuVisible(){
        binding.filterLayout.filterParentLayout.visibility = View.VISIBLE
    }

}
