package com.inzein.greenow.ui.main.fragments.recevents

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.inzein.greenow.common.LoadingState
import com.inzein.greenow.data.models.RecEvent
import com.inzein.greenow.data.repositories.EventRepository

class EventsNearMeViewModel(
    private val repository: EventRepository
) : ViewModel() {

    private lateinit var recEvents: LiveData<List<RecEvent>>

    lateinit var locationBasedSearch: String
    var filterType: Int = 0

    private val _loading = MutableLiveData<LoadingState>()
    val loading: LiveData<LoadingState>
        get() = _loading

    fun setLocationFilterType(selectedFilterType: Int){
        filterType = selectedFilterType
    }

    fun getRecEvents(): LiveData<List<RecEvent>> {
        _loading.postValue(LoadingState.LOADING)
        recEvents =   repository.getAllEvents()
        _loading.postValue(LoadingState.LOADED)

        return recEvents
    }


//    fun getRecEventsFiltered(
//        latitude: Double,
//        longitude: Double,
//        geocoder: Geocoder
//    ): LiveData<List<RecEvent>>{
//        _loading.postValue(LoadingState.LOADING)
//
////        when (filterType) {
////            0 -> {
////                locationBasedSearch =
////                    geocoder.getFromLocation(latitude, longitude, 1).get(0).locality
////                recEvents = repository.getAllEventsInMyCity(locationBasedSearch)
////                _loading.postValue(LoadingState.LOADED)
////            }
////            1 -> {
////                locationBasedSearch =
////                    geocoder.getFromLocation(latitude, longitude, 1).get(0).adminArea
////                recEvents = repository.getAllEventsInMyCounty(locationBasedSearch)
////                _loading.postValue(LoadingState.LOADED)
////            }
////            2 -> {
////                locationBasedSearch =
////                    geocoder.getFromLocation(latitude, longitude, 1).get(0).countryName
////                recEvents = repository.getAllEventsInMyCountry(locationBasedSearch)
////                _loading.postValue(LoadingState.LOADED)
////            }
////            else -> {
////                locationBasedSearch =
////                    geocoder.getFromLocation(latitude, longitude, 1).get(0).locality
////                recEvents = repository.getAllEventsInMyCity(locationBasedSearch)
////                _loading.postValue(LoadingState.LOADED)
////            }
////        }
//
//        repository.getAllEvents()
//
//        return recEvents
//    }
}