package com.inzein.greenow.ui.main.fragments.recyentitydetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.inzein.greenow.common.ReviewVisibiltyCase
import com.inzein.greenow.data.models.RecyclingEntity
import com.inzein.greenow.data.models.Review
import com.inzein.greenow.data.repositories.ReviewRepository
import com.inzein.greenow.data.repositories.UserRepository
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.launch

class RecycleEntityDetailsViewModel(
    private val reviewRepository: ReviewRepository,
    private val userRepository: UserRepository
): ViewModel() {

    lateinit var recycleEntity: RecyclingEntity
    var rating: Int = 0
    var message: String = ""

    private val _reviewsVisibilyState = MutableLiveData<ReviewVisibiltyCase>()
    val reviewsVisibilyState: LiveData<ReviewVisibiltyCase>
        get() = _reviewsVisibilyState



    fun getImagesStringUrlListFromRecycleEntity() : List<String>{

        val urlsList : MutableList<String> = mutableListOf()

        if (recycleEntity.image1.isNotEmpty()){
            urlsList.add(recycleEntity.image1)
        }
        if (recycleEntity.image2.isNotEmpty()){
            urlsList.add(recycleEntity.image2)
        }
        if (recycleEntity.image3.isNotEmpty()){
            urlsList.add(recycleEntity.image3)
        }

        return urlsList
    }


    fun addNewReview() {
        val currentUserUid = userRepository.currentUser()?.uid.toString()
        reviewRepository.addNewReview(currentUserUid, recycleEntity.id, recycleEntity.companyLevel,
            userRepository.getCurrentUserDetailsFromFirestore(currentUserUid).value?.profilePictureUrl!!, rating, message)
    }

    fun getAllReviews(): LiveData<List<Review>>{
        _reviewsVisibilyState.postValue(ReviewVisibiltyCase.LOADING_REVIEWS)
        val allReviewsForEntity =  reviewRepository.getAllReviewOfRecycleEntityByid(recycleEntity.id)
        _reviewsVisibilyState.postValue(ReviewVisibiltyCase.REVIEWS_LOADED)
        return allReviewsForEntity
    }

    fun setNoReviewsVisibilyCase(){
        _reviewsVisibilyState.postValue(ReviewVisibiltyCase.NO_REVIEWS)
    }

}