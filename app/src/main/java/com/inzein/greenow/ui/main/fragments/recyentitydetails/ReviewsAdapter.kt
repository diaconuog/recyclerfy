package com.inzein.greenow.ui.main.fragments.recyentitydetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.inzein.greenow.data.models.Review
import com.inzein.greenow.databinding.ReviewItemBinding

class ReviewsAdapter : ListAdapter<Review, ReviewsAdapter.ReviewViewHolder>(Companion) {

    class ReviewViewHolder(val binding : ReviewItemBinding) : RecyclerView.ViewHolder(binding.root)

    companion object: DiffUtil.ItemCallback<Review>() {
        override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean = oldItem.id == newItem.id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ReviewItemBinding.inflate(layoutInflater)

        return ReviewViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        val currentReview = getItem(position)

        holder.binding.review = currentReview
        holder.binding.executePendingBindings()
    }


}