package com.inzein.greenow.ui.main.fragments.aboutgreenow

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.inzein.greenow.R
import com.inzein.greenow.databinding.AboutGreenowFragmentBinding
import com.inzein.greenow.di.kodeinViewModel
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

class AboutGreenowFragment : Fragment(), KodeinAware{

    companion object {
        fun newInstance() = AboutGreenowFragment()
    }

    override val kodein by kodein()

    private val viewModel: AboutGreenowViewModel by kodeinViewModel()
    private lateinit var binding : AboutGreenowFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.about_greenow_fragment,
            container,
            false)


        return binding.root
    }

}