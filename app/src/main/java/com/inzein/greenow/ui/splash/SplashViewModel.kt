package com.inzein.greenow.ui.splash

import androidx.lifecycle.ViewModel
import com.inzein.greenow.data.repositories.UserRepository

class SplashViewModel(
    private val repository: UserRepository
) : ViewModel() {

    val user by lazy {
        repository.currentUser()
    }

    fun initialzeFirebaseMesseging() = repository.initialzeFirebaseMesseging()

}