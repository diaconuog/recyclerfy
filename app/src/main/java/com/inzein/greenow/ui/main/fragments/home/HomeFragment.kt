package com.inzein.greenow.ui.main.fragments.home

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.inzein.greenow.R
import com.inzein.greenow.common.DataBindingHelper.Companion.DEFAULT_ANIMATION_DURATION
import com.inzein.greenow.common.DataBindingHelper.Companion.scrollToBottom
import com.inzein.greenow.databinding.HomeFragmentBinding
import com.inzein.greenow.ui.main.fragments.map.RecyclingEntitiesMapFragment
import com.inzein.greenow.ui.main.fragments.recevents.EventsNeerMeFragment
import com.inzein.greenow.ui.main.fragments.recyclingentitis.RecyclingEntitiesNearByFragment
import com.inzein.greenow.ui.main.replaceFragment
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

class HomeFragment : Fragment(), KodeinAware {

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()

    }

    override val kodein by kodein()
    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.home_fragment,
            container,
            false)


        binding.discoverButton.setOnClickListener {
            (activity as AppCompatActivity).replaceFragment(
                RecyclingEntitiesMapFragment.newInstance())
        }

        binding.participateButton.setOnClickListener {
            (activity as AppCompatActivity).replaceFragment(
                EventsNeerMeFragment.newInstance())
        }

        binding.recycleButton.setOnClickListener {
            (activity as AppCompatActivity).replaceFragment(                RecyclingEntitiesNearByFragment.newInstance())

        }

        binding.handWithYellowBottleImageView.setOnClickListener {
            binding.handWithYellowBottleImageView.visibility = INVISIBLE
            binding.handAloneImageView.visibility = VISIBLE
            binding.bottleAloneImageView.visibility = VISIBLE
            onStartAnimation()
        }

        return binding.root
    }



    private fun onStartAnimation(){

        val displayMetrics = DisplayMetrics()

        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val screenHeight = displayMetrics.heightPixels.toFloat()

        val positionAnimator = ValueAnimator.ofFloat(0f, screenHeight)

        positionAnimator.addUpdateListener {
            val value = it.animatedValue as Float
            binding.bottleAloneImageView.translationY = value
        }

        val rotationAnimator = ObjectAnimator.ofFloat(binding.bottleAloneImageView, "rotation", 0f, 180f)
        val animatorSet = AnimatorSet()
        animatorSet.play(positionAnimator).with(rotationAnimator)
        animatorSet.duration = DEFAULT_ANIMATION_DURATION

        animatorSet.start()

        binding.scrollviewHolder.scrollToBottom()
    }


}
