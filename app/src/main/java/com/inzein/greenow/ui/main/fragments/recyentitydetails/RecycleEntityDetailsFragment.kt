package com.inzein.greenow.ui.main.fragments.recyentitydetails

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.inzein.greenow.R
import com.inzein.greenow.data.models.RecyclingEntity
import com.inzein.greenow.databinding.FragmentRecycleEntityDetailsBinding
import com.inzein.greenow.di.kodeinViewModel
import com.inzein.greenow.ui.login.replaceFragment
import com.inzein.greenow.ui.main.fragments.map.RecyclingEntitiesMapFragment
import com.inzein.greenow.ui.main.fragments.recevents.EventsNeerMeFragment
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein


class RecycleEntityDetailsFragment : Fragment(), KodeinAware {

    companion object {

        private const val RECYCLING_ENTITY : String = "recycling_entity"
        private const val RIPPLE_EFFECT_TIME_OUT = 4000


        @JvmStatic
        fun newInstance(recyclingEntity : RecyclingEntity): RecycleEntityDetailsFragment {

            val args = Bundle()
            args.putSerializable(RECYCLING_ENTITY, recyclingEntity)

            val recycleEntityDetailsFragment = RecycleEntityDetailsFragment()
            recycleEntityDetailsFragment.arguments = args

            return recycleEntityDetailsFragment
        }
    }

    override val kodein by kodein()
    private val viewModel: RecycleEntityDetailsViewModel by kodeinViewModel()
    private lateinit var binding: FragmentRecycleEntityDetailsBinding

    private val reviewsAdapter = ReviewsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Handler().postDelayed({
            binding.rippleBackground.startRippleAnimation()
        }, RIPPLE_EFFECT_TIME_OUT.toLong())

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_recycle_entity_details,
            container,
            false)

        binding.apply {
            lifecycleOwner = this@RecycleEntityDetailsFragment
        }
        binding.recycleEntityDetailViewModel = viewModel

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
        }

        viewModel.recycleEntity = arguments?.getSerializable(RECYCLING_ENTITY) as RecyclingEntity

        binding.viewPager.adapter = ImageViewPagerAdapter(context!!, viewModel.getImagesStringUrlListFromRecycleEntity())

        binding.tabLayout.setupWithViewPager(binding.viewPager)

        binding.callButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:${viewModel.recycleEntity.phoneNumber}"))
            startActivity(intent)
        }

        binding.websiteButton.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(viewModel.recycleEntity.entityLink)))
        }

        binding.facebookProfileButton.setOnClickListener {
            try {
                activity?.packageManager?.getPackageInfo("com.facebook.katana", 0)
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("fb://facewebmodal/f?href=${viewModel.recycleEntity.facebookLink}")))
            } catch (e : Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/${viewModel.recycleEntity.facebookLink}")))
            }
        }

        binding.seeEventsButton.setOnClickListener {
            (activity as AppCompatActivity).replaceFragment(EventsNeerMeFragment.newInstance())
        }

        binding.seeAllPointButton.setOnClickListener {
            (activity as AppCompatActivity).replaceFragment(RecyclingEntitiesMapFragment.newInstance())
        }

        binding.reviewsAdapter = reviewsAdapter


        viewModel.getAllReviews().observe(viewLifecycleOwner, Observer {
            val reviewsList = it.toMutableList()

            if (reviewsList.size > 0) {
                reviewsAdapter.submitList(reviewsList)
            } else {
                viewModel.setNoReviewsVisibilyCase()
            }
        })

        binding.seeAllPointButton.setOnClickListener {
            (activity as AppCompatActivity).replaceFragment(RecyclingEntitiesMapFragment.newInstance())
        }

        binding.seeEventsButton.setOnClickListener {
            (activity as AppCompatActivity).replaceFragment(EventsNeerMeFragment.newInstance())
        }

        binding.addReviewTv.setOnClickListener {

        }

        return binding.root
    }

}
