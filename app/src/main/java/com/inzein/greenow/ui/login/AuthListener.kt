package com.inzein.greenow.ui.login

interface AuthListener {
    fun onStarted()
    fun onSuccess()
    fun onFailure(message: String)
}