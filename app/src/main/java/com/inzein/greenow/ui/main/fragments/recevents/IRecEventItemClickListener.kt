package com.inzein.greenow.ui.main.fragments.recevents

import com.inzein.greenow.data.models.RecEvent

interface IRecEventItemClickListener {
    fun onRecEventItemClicked(recEvent: RecEvent)
}