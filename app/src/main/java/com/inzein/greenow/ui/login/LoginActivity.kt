package com.inzein.greenow.ui.login

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.inzein.greenow.R
import com.inzein.greenow.common.BaseActivity
import com.inzein.greenow.common.DataBindingHelper.Companion.hideKeyboard
import com.inzein.greenow.databinding.ActivityLoginBinding
import com.inzein.greenow.di.kodeinViewModel
import com.inzein.greenow.ui.login.fragments.LoginFragment
import com.inzein.greenow.ui.login.fragments.RegisterViewModel
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein


class LoginActivity : BaseActivity(), KodeinAware{

    override val kodein by kodein()
    private val viewModel: RegisterViewModel by kodeinViewModel()

    companion object{
        fun getLaunchIntent(from: Context) = Intent(from, LoginActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }

        fun isNetworkConnected(context: Context) : Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val nw      = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        }

        fun isValidEmailTyped(email: String):Boolean {
            return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        binding.authViewModel = viewModel

        // Initially display the first fragment in main activity
        replaceFragment(LoginFragment.newInstance())

        binding.backgroundContraintLayout.setOnClickListener {
            it.hideKeyboard()
        }
    }

    override fun onBackPressed(){
        requireDoubleBackPressToExitApp()
    }

}

// Extension function to replace fragment
fun AppCompatActivity.replaceFragment(fragment:Fragment){
    val fragmentManager = supportFragmentManager
    val transaction = fragmentManager.beginTransaction()
    transaction.replace(R.id.fragment_container,fragment)
    transaction.addToBackStack(null)
    transaction.commit()
}





