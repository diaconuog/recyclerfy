package com.inzein.greenow.ui.main.fragments.recyclingentitis

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.inzein.greenow.data.models.RecyclingEntity
import com.inzein.greenow.databinding.RecyclingEntityItemBinding


class RecyclingEntitiesAdapter(val recyclerViewItemClickListener: IRecyclerViewItemClickListener): ListAdapter<RecyclingEntity, RecyclingEntitiesAdapter.RecyclingEntityViewHolder>(
    Companion
)
{

    class RecyclingEntityViewHolder(val binding : RecyclingEntityItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bindInterface(recyclingEntity: RecyclingEntity,
                          recyclerViewItemClickListener: IRecyclerViewItemClickListener){
            itemView.setOnClickListener{
                recyclerViewItemClickListener.onRecyclerviewItemClicked(recyclingEntity)
            }
        }
    }

    companion object: DiffUtil.ItemCallback<RecyclingEntity>() {
        override fun areItemsTheSame(oldItem: RecyclingEntity, newItem: RecyclingEntity): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: RecyclingEntity, newItem: RecyclingEntity): Boolean = oldItem.id == newItem.id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclingEntityViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RecyclingEntityItemBinding.inflate(layoutInflater)

        return RecyclingEntityViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: RecyclingEntityViewHolder, position: Int) {
        val currentRecyclingEntity = getItem(position)
        holder.binding.recyclingEntity = currentRecyclingEntity
        holder.binding.executePendingBindings()

        holder.bindInterface(currentRecyclingEntity, recyclerViewItemClickListener)
    }

}