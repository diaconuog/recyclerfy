package com.inzein.greenow.ui.login.fragments

import androidx.lifecycle.ViewModel
import com.inzein.greenow.data.repositories.UserRepository
import com.inzein.greenow.ui.login.AuthListener
import com.inzein.greenow.ui.login.LoginActivity.Companion.isValidEmailTyped
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LoginViewModel (
    private val repository: UserRepository
) : ViewModel() {
    //email and password for the input
    var email: String? = null
    var password: String? = null

    //auth listener
    var authListener: AuthListener? = null

    //disposable to dispose the Completable
    private val disposables = CompositeDisposable()

    val user by lazy {
        repository.currentUser()
    }

    //function to perform login
    fun loginUserWithEmailAndPassword() {

        //validating email and password
        if (email.isNullOrEmpty() || password.isNullOrEmpty() || isValidEmailTyped(email!!)) {
            authListener?.onFailure("Invalid email or password")
            return
        }

        //authentication started
        authListener?.onStarted()

        //calling login from repository to perform the actual authentication
        val disposable = repository.login(email!!, password!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                //sending a success callback
                authListener?.onSuccess()
            }, {
                //sending a failure callback
                authListener?.onFailure(it.message!!)
            })
        disposables.add(disposable)
    }

    fun signInUserWithGoogleAccount(acct : GoogleSignInAccount){

        if (acct.isExpired){
            authListener?.onFailure("Google Account SignIn Error")
            return
        }

        authListener?.onStarted()

        val disposable = repository.signInWithGoogle(acct)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                authListener?.onSuccess()
                addGoogleUserToFirestore()
            }, {
                authListener?.onFailure(it.message!!)
            })
        disposables.add(disposable)
    }

    fun loginWithFacebookAccesToken(token: AccessToken) {
        val disposable = repository.loginWithFacebook(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                authListener?.onSuccess()
            }, {
                authListener?.onFailure(it.message!!)
            })
        disposables.add(disposable)
    }


    fun addNewUserToFirebaseFirestore() = repository.addNewUserToFirestoreStandard()

    fun addNewFacebookUserToFirestore(firstName: String, lastName: String) =
        repository.addNewFacebookUserToFirestore(firstName, lastName)

    fun addGoogleUserToFirestore() = repository.addGoogleUserToFirestore()


    //disposing the disposables
    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}