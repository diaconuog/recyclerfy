package com.inzein.greenow.ui.main.fragments.map

import android.app.Activity
import android.view.View
import androidx.databinding.DataBindingUtil
import com.inzein.greenow.R
import com.inzein.greenow.data.models.RecyclingEntity
import com.inzein.greenow.databinding.CustomMarkerInfoWindowBinding
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker


class CustomMarkerInfoWindowAdapter(private val activity: Activity): GoogleMap.InfoWindowAdapter {
    private lateinit var binding : CustomMarkerInfoWindowBinding


    override fun getInfoContents(p0: Marker?): View {
        binding = DataBindingUtil.setContentView(activity, R.layout.custom_marker_info_window)

        binding.recyclingEntity = p0!!.tag as RecyclingEntity

        return binding.root
    }

    override fun getInfoWindow(p0: Marker?): View {
        return activity.layoutInflater.inflate(R.layout.custom_marker_info_window, null)
    }
}