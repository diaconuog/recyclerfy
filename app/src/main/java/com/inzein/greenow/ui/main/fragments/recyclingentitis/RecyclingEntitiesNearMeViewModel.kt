package com.inzein.greenow.ui.main.fragments.recyclingentitis

import android.location.Geocoder
import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.inzein.greenow.common.LoadingState
import com.inzein.greenow.data.models.RecyclingEntity
import com.inzein.greenow.data.repositories.RecyclingEntityRepository

class RecyclingEntitiesNearMeViewModel(
    private val repository: RecyclingEntityRepository
) : ViewModel() {


    private lateinit var recyclingEntities: LiveData<List<RecyclingEntity>>

    lateinit var locationBasedSearch: String
    var filterType : Int = -1

    private val _loading = MutableLiveData<LoadingState>()
    val loading: LiveData<LoadingState>
        get() = _loading

    lateinit var location: Location
    var filterOnlyEntitiesWithPickUp = false
    var recycleMaterialToFilerFor : String = ""

    fun applyFilters(geocoder: Geocoder){
        getRecyclingCompaniesFiltered(geocoder)
    }

    fun getRecyclingCompaniesFiltered(
        geocoder: Geocoder
    ): LiveData<List<RecyclingEntity>> {

        _loading.postValue(LoadingState.LOADING)

        if (!this::location.isInitialized){
            filterType = -1
        }

        when (filterType) {
            0 -> {
                locationBasedSearch =
                    geocoder.getFromLocation(location.latitude, location.longitude, 1).get(0).locality.toString()
                recyclingEntities = repository.getAllRecyclingEntitiessFromMyCity(locationBasedSearch, filterOnlyEntitiesWithPickUp)
                _loading.postValue(LoadingState.LOADED)
            }
            1 -> {
                locationBasedSearch =
                    geocoder.getFromLocation(location.latitude, location.longitude, 1).get(0).adminArea.toString()
                recyclingEntities = repository.getAllRecyclingEntitiesFromMyCounty(locationBasedSearch, filterOnlyEntitiesWithPickUp)
                _loading.postValue(LoadingState.LOADED)
            }
            2 -> {
                locationBasedSearch =
                    geocoder.getFromLocation(location.latitude, location.longitude, 1).get(0).countryCode.toString()
                recyclingEntities = repository.getAllRecyclingEntitiesFromMyCountry(locationBasedSearch, filterOnlyEntitiesWithPickUp)
                _loading.postValue(LoadingState.LOADED)
            }
            else -> {
                locationBasedSearch =
                    geocoder.getFromLocation(location.latitude, location.longitude, 1).get(0).locality
                recyclingEntities = repository.getAllRecyclingEntities()
                _loading.postValue(LoadingState.LOADED)
            }
        }

        return recyclingEntities
    }



}