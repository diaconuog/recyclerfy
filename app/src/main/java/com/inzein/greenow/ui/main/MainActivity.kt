package com.inzein.greenow.ui.main

import android.os.Bundle
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.inzein.greenow.R
import com.inzein.greenow.common.BaseActivity
import com.inzein.greenow.databinding.ActivityMainBinding
import com.inzein.greenow.ui.login.LoginActivity
import com.inzein.greenow.ui.main.fragments.aboutgreenow.AboutGreenowFragment
import com.inzein.greenow.ui.main.fragments.eventdetails.EventDetailsFragment
import com.inzein.greenow.ui.main.fragments.home.HomeFragment
import com.inzein.greenow.ui.main.fragments.map.RecyclingEntitiesMapFragment
import com.inzein.greenow.ui.main.fragments.recevents.EventsNeerMeFragment
import com.inzein.greenow.ui.main.fragments.recyclingentitis.RecyclingEntitiesNearByFragment
import com.inzein.greenow.ui.main.fragments.recyentitydetails.RecycleEntityDetailsFragment
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.navigation_drawer_content_main.view.*

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var binding : ActivityMainBinding

    private lateinit var visibileFragment : Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setSupportActionBar(binding.navigationDrawerContentMain.toolbar)

        val toggle = ActionBarDrawerToggle(
            this, binding.drawerLayout, binding.navigationDrawerContentMain.toolbar, 0, 0
        )

        toggle.isDrawerIndicatorEnabled = false

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        binding.navigationDrawerContentMain.nav_drawer_icon_image_view.setOnClickListener {
            if (binding.navView.isVisible){
                binding.drawerLayout.closeDrawer(GravityCompat.START)
            } else {
                binding.drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        binding.navigationDrawerContentMain.filter_image_button.setOnClickListener {
            if (visibileFragment is RecyclingEntitiesNearByFragment){
                (visibileFragment as RecyclingEntitiesNearByFragment).makeFilterMenuVisible()
            }
        }

        binding.navigationDrawerContentMain.back_animated_button.setOnClickListener {
            onBackPressed()
            showGeneralNavigationDrawerType()
        }

        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        binding.navView.setNavigationItemSelectedListener(this)


        supportActionBar!!.elevation = 0.0f

        replaceFragment(HomeFragment.newInstance())
        showHomeScreenNavigationDrawerType()

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_entities -> {
                visibileFragment = RecyclingEntitiesNearByFragment.newInstance()
                replaceFragment(visibileFragment)
            }
            R.id.nav_events -> {
                visibileFragment = EventsNeerMeFragment.newInstance()
                replaceFragment(visibileFragment)
            }
            R.id.nav_map -> {
                visibileFragment = RecyclingEntitiesMapFragment.newInstance()
                replaceFragment(visibileFragment)
            }
            R.id.nav_about_greenow -> {
                visibileFragment = AboutGreenowFragment.newInstance()
                replaceFragment(visibileFragment)
            }
            R.id.nav_logout -> {
                signOutUser()
            }
            R.id.home_screen -> {
                visibileFragment = HomeFragment.newInstance()
                replaceFragment(visibileFragment)
            }
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)

        if (fragment is RecycleEntityDetailsFragment || fragment is EventDetailsFragment){
            showBackNavigationDrawerType()
        } else {
            if (fragment is HomeFragment || fragment is AboutGreenowFragment){
                showHomeScreenNavigationDrawerType()
            } else {
                if (fragment is EventsNeerMeFragment){
                    showGeneralNavigationDrawerTypeWithoutFilter()
                } else {
                    showGeneralNavigationDrawerType()
                }
            }
        }

    }

    private fun signOutUser(){
        startActivity(LoginActivity.getLaunchIntent(this))
        FirebaseAuth.getInstance().signOut()
    }

    private fun showHomeScreenNavigationDrawerType(){
        binding.navigationDrawerContentMain.app_title_text_view.visibility = GONE
        binding.navigationDrawerContentMain.filter_image_button.visibility = GONE
        binding.navigationDrawerContentMain.app_bar_layout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorTransparent))
        binding.navigationDrawerContentMain.back_animated_button.visibility = GONE
        binding.navigationDrawerContentMain.nav_drawer_icon_image_view.visibility = VISIBLE
    }

    internal fun showGeneralNavigationDrawerType(){
        binding.navigationDrawerContentMain.app_title_text_view.visibility = VISIBLE
        binding.navigationDrawerContentMain.filter_image_button.visibility = VISIBLE
        binding.navigationDrawerContentMain.app_bar_layout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        binding.navigationDrawerContentMain.back_animated_button.visibility = GONE
        binding.navigationDrawerContentMain.nav_drawer_icon_image_view.visibility = VISIBLE
    }

    private fun showGeneralNavigationDrawerTypeWithoutFilter(){
        binding.navigationDrawerContentMain.app_title_text_view.visibility = VISIBLE
        binding.navigationDrawerContentMain.filter_image_button.visibility = GONE
        binding.navigationDrawerContentMain.app_bar_layout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        binding.navigationDrawerContentMain.back_animated_button.visibility = GONE
        binding.navigationDrawerContentMain.nav_drawer_icon_image_view.visibility = VISIBLE
    }

    private fun showBackNavigationDrawerType(){
        binding.navigationDrawerContentMain.app_title_text_view.visibility = GONE
        binding.navigationDrawerContentMain.filter_image_button.visibility = GONE
        binding.navigationDrawerContentMain.nav_drawer_icon_image_view.visibility = GONE
        binding.navigationDrawerContentMain.back_animated_button.visibility = VISIBLE
    }
}

// Extension function to replace fragment
fun AppCompatActivity.replaceFragment(fragment:Fragment){
    val fragmentManager = supportFragmentManager
    val transaction = fragmentManager.beginTransaction()
    transaction.replace(R.id.fragment_container,fragment)
    transaction.addToBackStack(null)
    transaction.commit()
}