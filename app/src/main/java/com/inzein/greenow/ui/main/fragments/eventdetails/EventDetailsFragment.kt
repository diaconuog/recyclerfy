package com.inzein.greenow.ui.main.fragments.eventdetails

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.inzein.greenow.R
import com.inzein.greenow.data.models.RecEvent
import com.inzein.greenow.databinding.EventDetailsFragmentBinding
import com.inzein.greenow.databinding.FragmentEventsBinding
import com.inzein.greenow.databinding.FragmentRecycleEntityDetailsBinding
import com.inzein.greenow.di.kodeinViewModel
import com.inzein.greenow.ui.main.fragments.recyentitydetails.ImageViewPagerAdapter
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

class EventDetailsFragment() : Fragment(), KodeinAware {

    companion object {

        private const val REC_EVENT : String = "rec_event"
        private const val RIPPLE_EFFECT_TIME_OUT = 4000


        @JvmStatic
        fun newInstance(recEvent: RecEvent): EventDetailsFragment {

            val args = Bundle()
            args.putSerializable(REC_EVENT, recEvent)

            val recEventDetailsFragment = EventDetailsFragment()
            recEventDetailsFragment.arguments = args

            return recEventDetailsFragment
        }
    }

    override val kodein by kodein()

    private val viewModel: EventDetailsViewModel by kodeinViewModel()
    private lateinit var binding: EventDetailsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.event_details_fragment,
            container,
            false)

        binding.apply {
            lifecycleOwner = this@EventDetailsFragment
        }

        binding.eventDetailViewModel = viewModel

        viewModel.event = arguments?.getSerializable(EventDetailsFragment.REC_EVENT) as RecEvent

        binding.viewPager.adapter = ImageViewPagerAdapter(context!!, viewModel.getImagesStringUrlListFromRecycleEntity())

        binding.tabLayout.setupWithViewPager(binding.viewPager)

        binding.websiteButton.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(viewModel.event.recEventLink)))
        }

        binding.facebookProfileButton.setOnClickListener {
            try {
                activity?.packageManager?.getPackageInfo("com.facebook.katana", 0)
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("fb://facewebmodal/f?href=${viewModel.event.facebookLink}")))
            } catch (e : Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/${viewModel.event.facebookLink}")))
            }
        }

        return binding.root
    }

}
