package com.inzein.greenow.ui.main.fragments.recevents

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.inzein.greenow.data.models.RecEvent
import com.inzein.greenow.databinding.RecEventItemBinding

class RecEventsAdapter(val recEventItemClickListener: IRecEventItemClickListener): ListAdapter<RecEvent, RecEventsAdapter.RecEventViewHolder>(
    Companion
){

    class RecEventViewHolder(val binding : RecEventItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bindInterface(recEvent: RecEvent,  recEventItemClickListener: IRecEventItemClickListener){
            itemView.setOnClickListener {
                recEventItemClickListener.onRecEventItemClicked(recEvent)
            }
        }
    }

    companion object: DiffUtil.ItemCallback<RecEvent>() {
        override fun areItemsTheSame(oldItem: RecEvent, newItem: RecEvent): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: RecEvent, newItem: RecEvent): Boolean = oldItem.id == newItem.id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecEventViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RecEventItemBinding.inflate(layoutInflater)

        return RecEventViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: RecEventViewHolder, position: Int) {
        val currentRecEvent = getItem(position)
        holder.binding.recEvent = currentRecEvent
        holder.binding.executePendingBindings()

        holder.bindInterface(currentRecEvent, recEventItemClickListener)
    }
}