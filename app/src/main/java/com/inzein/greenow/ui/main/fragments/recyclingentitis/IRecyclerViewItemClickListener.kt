package com.inzein.greenow.ui.main.fragments.recyclingentitis

import com.inzein.greenow.data.models.RecyclingEntity

interface IRecyclerViewItemClickListener {
    fun onRecyclerviewItemClicked(recyclingEntity : RecyclingEntity)
}