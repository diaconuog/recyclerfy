package com.inzein.greenow.ui.main.fragments.eventdetails

import androidx.lifecycle.ViewModel
import com.inzein.greenow.data.models.RecEvent

class EventDetailsViewModel : ViewModel() {

    lateinit var event: RecEvent

    fun getImagesStringUrlListFromRecycleEntity() : List<String>{

        val urlsList : MutableList<String> = mutableListOf()

        if (event.image1.isNotEmpty()){
            urlsList.add(event.image1)
        }
        if (event.image2.isNotEmpty()){
            urlsList.add(event.image2)
        }
        if (event.image3.isNotEmpty()){
            urlsList.add(event.image3)
        }

        return urlsList
    }
}
