package com.inzein.greenow.common

import android.content.Context
import androidx.annotation.NonNull
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.google.firebase.storage.StorageReference
import org.jetbrains.annotations.NotNull
import java.io.InputStream

@GlideModule
class MyAppGlideModule: AppGlideModule(){

    override fun registerComponents(@NonNull context: Context, @NotNull glide: Glide,@NonNull registry: Registry) {
        registry.append(StorageReference::class.java, InputStream::class.java, FirebaseImageLoader.Factory())
    }
}