package com.inzein.greenow.common

import android.animation.ObjectAnimator
import android.net.Uri
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.ScrollView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.inzein.greenow.R
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso

class DataBindingHelper {

    companion object{

        const val DEFAULT_ANIMATION_DURATION = 2500L

        @BindingAdapter(value = ["setImageUrl"])
        @JvmStatic
        fun ImageView.bindImageUrl(url: String?){

            if (url != null && url.isNotBlank()){
                val iconRef: StorageReference =
                    FirebaseStorage.getInstance().getReferenceFromUrl(url)

                iconRef.downloadUrl.loadIntoPicasso(this)
            }
        }

        @JvmStatic
        @BindingAdapter(value = ["setAdapter"])
        fun RecyclerView.bindRecyclerViewAdapter(adapter: RecyclerView.Adapter<*>){
            this.run {
                this.adapter = adapter
            }
        }

        @JvmStatic
        @BindingAdapter(value = ["setupVisibility"])
        fun ProgressBar.progressVisibility(loadingState: LoadingState?){
            loadingState?.let {
                this.isVisible = when(it.status){
                    LoadingState.Status.RUNNING -> true
                    LoadingState.Status.SUCCESS -> false
                    LoadingState.Status.FAILED -> false
                }
            }
        }

        @JvmStatic
        @BindingAdapter(value = ["reviewViewVisibilityByCase"])
        fun RecyclerView.reviewViewVisibilityByCase(reviewVisibiltyCase: ReviewVisibiltyCase?){
            reviewVisibiltyCase?.let {
                this.visibility = when(it.visibilityCase){
                    ReviewVisibiltyCase.VisibilityCase.LOADING_REVIEWS -> GONE
                    ReviewVisibiltyCase.VisibilityCase.REVIEWS_LOADED -> VISIBLE
                    ReviewVisibiltyCase.VisibilityCase.NO_REVIEWS -> GONE
                }
            }
        }

        @JvmStatic
        @BindingAdapter(value = ["reviewViewVisibilityByCase"])
        fun ProgressBar.reviewViewVisibilityByCase(reviewVisibiltyCase: ReviewVisibiltyCase?){
            reviewVisibiltyCase?.let {
                this.visibility = when(it.visibilityCase){
                    ReviewVisibiltyCase.VisibilityCase.LOADING_REVIEWS -> VISIBLE
                    ReviewVisibiltyCase.VisibilityCase.REVIEWS_LOADED -> GONE
                    ReviewVisibiltyCase.VisibilityCase.NO_REVIEWS -> GONE
                }
            }
        }

        @JvmStatic
        @BindingAdapter(value = ["reviewViewVisibilityByCase"])
        fun TextView.reviewViewVisibilityByCase(reviewVisibiltyCase: ReviewVisibiltyCase?){
            reviewVisibiltyCase?.let {
                this.visibility = when(it.visibilityCase){
                    ReviewVisibiltyCase.VisibilityCase.LOADING_REVIEWS -> GONE
                    ReviewVisibiltyCase.VisibilityCase.REVIEWS_LOADED -> GONE
                    ReviewVisibiltyCase.VisibilityCase.NO_REVIEWS -> VISIBLE
                }
            }
        }

        fun Task<Uri>.loadIntoPicasso(imageView: ImageView){
            addOnSuccessListener {
                Picasso.get()
                    .load(it)
                    .placeholder(R.drawable.logo_placeholder)
                    .into(imageView)
            }
        }

        //todo medium
        @JvmStatic
        @BindingAdapter(value = ["clipToOutline"])
        fun ConstraintLayout.clipToOutline(boolean: Boolean){

            this.clipToOutline = boolean
        }

        @JvmStatic
        fun ScrollView.scrollToBottom() {
            val lastChild = getChildAt(childCount - 1)
            val bottom = lastChild.bottom + paddingBottom
            val delta = bottom - (scrollY+ height)
            ObjectAnimator.ofInt(this, "scrollY", delta).setDuration(DEFAULT_ANIMATION_DURATION).start()
        }

        @JvmStatic
        fun View.hideKeyboard() {
            val inputMethodManager = context!!.getSystemService(android.content.Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            inputMethodManager?.hideSoftInputFromWindow(this.windowToken, 0)
        }
    }

}