package com.inzein.greenow.common


@Suppress("DataClassPrivateConstructor")
data class ReviewVisibiltyCase private constructor(public val visibilityCase: VisibilityCase){

    companion object {
        val LOADING_REVIEWS = ReviewVisibiltyCase(VisibilityCase.LOADING_REVIEWS)
        val REVIEWS_LOADED = ReviewVisibiltyCase(VisibilityCase.REVIEWS_LOADED)
        val NO_REVIEWS = ReviewVisibiltyCase(VisibilityCase.NO_REVIEWS)
    }

    enum class VisibilityCase{
        LOADING_REVIEWS,
        REVIEWS_LOADED,
        NO_REVIEWS
    }
}