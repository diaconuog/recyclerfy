package com.inzein.greenow.common

import android.app.Application
import com.inzein.greenow.R
import com.inzein.greenow.data.firebase.*
import com.inzein.greenow.data.repositories.EventRepository
import com.inzein.greenow.data.repositories.RecyclingEntityRepository
import com.inzein.greenow.data.repositories.ReviewRepository
import com.inzein.greenow.data.repositories.UserRepository
import com.inzein.greenow.di.ViewModelFactory
import com.inzein.greenow.di.bindViewModel
import com.inzein.greenow.ui.login.fragments.LoginViewModel
import com.inzein.greenow.ui.login.fragments.RegisterViewModel
import com.inzein.greenow.ui.login.fragments.ResetPasswordViewModel
import com.inzein.greenow.ui.main.fragments.aboutgreenow.AboutGreenowViewModel
import com.inzein.greenow.ui.main.fragments.eventdetails.EventDetailsViewModel
import com.inzein.greenow.ui.main.fragments.map.RecyclingEntitiesMapViewModel
import com.inzein.greenow.ui.main.fragments.recevents.EventsNearMeViewModel
import com.inzein.greenow.ui.main.fragments.recyclingentitis.RecyclingEntitiesNearMeViewModel
import com.inzein.greenow.ui.main.fragments.recyentitydetails.RecycleEntityDetailsViewModel
import com.inzein.greenow.ui.splash.SplashViewModel
import com.google.android.libraries.places.api.Places
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.direct
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import java.util.*

class GreenowApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@GreenowApplication))

        bind() from singleton { FirebaseUsersSource() }
        bind() from singleton { FirebaseRecyclingEntitiesSource() }
        bind() from singleton { FirebaseEventsSource() }
        bind() from singleton { FirebaseSourceBase() }
        bind() from singleton { FirebaseReviewsSource() }
        bind() from singleton { UserRepository(instance(), instance()) }
        bind() from singleton { RecyclingEntityRepository(instance()) }
        bind() from singleton { EventRepository(instance()) }
        bind() from singleton { ReviewRepository(instance()) }

        bind<ViewModelFactory>() with singleton  { ViewModelFactory(kodein.direct) }
        bindViewModel<RegisterViewModel>() with provider {
            RegisterViewModel(instance())
        }
        bindViewModel<SplashViewModel>() with provider {
            SplashViewModel(instance())
        }
        bindViewModel<LoginViewModel>() with provider {
            LoginViewModel(instance())
        }
        bindViewModel<ResetPasswordViewModel>() with provider {
            ResetPasswordViewModel(instance())
        }
        bindViewModel<RecyclingEntitiesNearMeViewModel>() with provider {
            RecyclingEntitiesNearMeViewModel(
                instance()
            )
        }
        bindViewModel<EventsNearMeViewModel>() with provider {
            EventsNearMeViewModel(
                instance()
            )
        }
        bindViewModel<RecyclingEntitiesMapViewModel>() with provider {
            RecyclingEntitiesMapViewModel(
                instance()
            )
        }
        bindViewModel<RecycleEntityDetailsViewModel>() with provider {
            RecycleEntityDetailsViewModel(instance(), instance())
        }
        bindViewModel<EventDetailsViewModel>() with provider {
            EventDetailsViewModel()
        }
        bindViewModel<AboutGreenowViewModel>() with provider {
            AboutGreenowViewModel()
        }
    }

    override fun onCreate() {
        super.onCreate()

        if(!Places.isInitialized()){
            Places.initialize(applicationContext, getString(R.string.google_api_key), Locale.ENGLISH)
        }
    }
}