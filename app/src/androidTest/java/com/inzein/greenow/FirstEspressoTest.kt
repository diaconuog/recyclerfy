package com.inzein.greenow

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.inzein.greenow.ui.login.LoginActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class FirstEspressoTest {

    @get: Rule
    val activityRule = ActivityTestRule(LoginActivity::class.java)

    @Test
    fun testingyolo(){
        onView(withId(R.id.welcome_tv)).check(matches(withText("Welcome")))
    }
}